using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using System.Linq;
using System.Collections.Generic;
using System;
using Android.Graphics;
using MvvmCross.Platform;
using MvvmCross.Plugins.File;
using System.IO;
using WhatSupp.Services;

namespace WhatSupp.ViewModels
{
    public class ProductScanDetailViewModel : MvxViewModel
    {
        private readonly IRestService _restService;

        public ProductScanDetailViewModel(IRestService restService)
        {
            _restService = restService;
        }

        public void Init(ProductScan item)
        {
            //Item = item;
            //var _fileService = Mvx.Resolve<IMvxFileStore>();
            //var memoryStream = new MemoryStream();
            //_fileService.OpenRead(item.FrontPath).CopyTo(memoryStream);
            //Bytes = memoryStream.ToArray();
        }

        private ProductScan _productScan;
        public ProductScan Item
        {
            get { return _productScan; }
            set { _productScan = value; RaisePropertyChanged(() => Item); }
        }

        private byte[] _bytes;
        public byte[] Bytes
        {
            get { return _bytes; }
            set { _bytes = value; RaisePropertyChanged(() => Bytes); }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set { _message = value; RaisePropertyChanged(() => Message); }
        }

        //private MvxCommand _uploadImageCommand;
        //public System.Windows.Input.ICommand UploadImageCommand
        //{
        //    get
        //    {
        //        _uploadImageCommand = _uploadImageCommand ?? new MvxCommand(UploadImage);
        //        return _uploadImageCommand;
        //    }
        //}

        //private async void UploadImage()
        //{
        //    Message = await _restService.UploadProductImage(Item.UPC, Bytes);
        //}
    }
}
