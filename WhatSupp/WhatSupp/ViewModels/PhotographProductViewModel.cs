using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using System.Linq;
using MvvmCross.Plugins.PictureChooser;
using System.IO;
using MvvmCross.Platform;
using MvvmCross.Plugins.File;
using System;

namespace WhatSupp.ViewModels
{
    public class PhotographProductViewModel : MvxViewModel
    {
        //private readonly IRepository<ProductScan> _scanRepo;
        //private readonly IMvxPictureChooserTask _pictureService;
        //private readonly IMvxFileStore _fileService;

        //public PhotographProductViewModel(
        //    IRepository<ProductScan> scanRepo,
        //    IMvxPictureChooserTask pictureService,
        //    IMvxFileStore fileService)
        //{
        //    _scanRepo = scanRepo;
        //    _pictureService = pictureService;
        //    _fileService = fileService;
        //}

        //public void Init(string upc)
        //{
        //    UPC = upc;
        //}

        //private string UPC { get; set; }

        //private MvxCommand _takeFrontPictureCommand;
        //public System.Windows.Input.ICommand TakeFrontPictureCommand
        //{
        //    get
        //    {
        //        _takeFrontPictureCommand = _takeFrontPictureCommand ?? new MvxCommand(DoTakeFrontPicture);
        //        return _takeFrontPictureCommand;
        //    }
        //}

        //private void DoTakeFrontPicture()
        //{
        //    _pictureService.TakePicture(400, 95, OnFrontPicture, () => { });
        //}

        //private MvxCommand _takeBackPictureCommand;
        //public System.Windows.Input.ICommand TakeBackPictureCommand
        //{
        //    get
        //    {
        //        _takeBackPictureCommand = _takeBackPictureCommand ?? new MvxCommand(DoTakeBackPicture);
        //        return _takeBackPictureCommand;
        //    }
        //}

        //private void DoTakeBackPicture()
        //{
        //    _pictureService.TakePicture(400, 95, OnBackPicture, () => { });
        //}

        //private byte[] _frontBytes;
        //public byte[] FrontBytes
        //{
        //    get { return _frontBytes; }
        //    set { _frontBytes = value; RaisePropertyChanged(() => FrontBytes); }
        //}

        //private byte[] _backBytes;
        //public byte[] BackBytes
        //{
        //    get { return _backBytes; }
        //    set { _backBytes = value; RaisePropertyChanged(() => BackBytes); }
        //}

        //private void OnFrontPicture(Stream pictureStream)
        //{
        //    var memoryStream = new MemoryStream();
        //    pictureStream.CopyTo(memoryStream);
        //    FrontBytes = memoryStream.ToArray();

        //    var id = Guid.NewGuid().ToString("N");

        //    var randomFileName = "Image" + id + ".jpg";
        //    _fileService.EnsureFolderExists("Images");
        //    var path = _fileService.PathCombine("Images", randomFileName);
        //    _fileService.WriteFile(path, FrontBytes);

        //    var scan = _scanRepo.Get(x => x.UPC == UPC);

        //    if (scan == null)
        //    {
        //        _scanRepo.Create(new ProductScan()
        //        {
        //            UPC = UPC,
        //            FrontPath = path
        //        });
        //    }
        //    else
        //    {
        //        scan.FrontPath = path;
        //        _scanRepo.Update(scan);
        //    }
        //}

        //private void OnBackPicture(Stream pictureStream)
        //{
        //    var memoryStream = new MemoryStream();
        //    pictureStream.CopyTo(memoryStream);
        //    BackBytes = memoryStream.ToArray();

        //    var id = Guid.NewGuid().ToString("N");

        //    var randomFileName = "Image" + id + ".jpg";
        //    _fileService.EnsureFolderExists("Images");
        //    var path = _fileService.PathCombine("Images", randomFileName);
        //    _fileService.WriteFile(path, BackBytes);

        //    var scan = _scanRepo.Get(x => x.UPC == UPC);

        //    if (scan == null)
        //    {
        //        _scanRepo.Create(new ProductScan()
        //        {
        //            UPC = UPC,
        //            BackPath = path
        //        });
        //    }
        //    else
        //    {
        //        scan.BackPath = path;
        //        _scanRepo.Update(scan);
        //    }
        //}
    }
}
