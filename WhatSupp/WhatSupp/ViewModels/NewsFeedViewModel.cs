using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using System.Linq;
using System;
using WhatSupp.Services;

namespace WhatSupp.ViewModels
{
    public class NewsFeedViewModel : MvxViewModel
    {
        private readonly IRestService _restService;

        public List<Test> Posts { get; set; }

        public NewsFeedViewModel(IRestService restService)
        {
            _restService = restService;
            Posts = new List<Test>();
        }

        public async void Init()
        {
            var posts = await _restService.GetPosts();

            Posts = posts.Cast<Test>().ToList();
        }

        private MvxCommand<Post> _postSelectedCommand;
        public System.Windows.Input.ICommand PostSelectedCommand
        {
            get
            {
                _postSelectedCommand = _postSelectedCommand ?? new MvxCommand<Post>(DoSelectPost);
                return _postSelectedCommand;
            }
        }

        private void DoSelectPost(Post post)
        {
            if (post != null)
            {
                ShowViewModel<ViewPostViewModel>(post);
            }
        }
    }
}
