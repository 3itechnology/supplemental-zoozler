using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using WhatSupp.Services;

namespace WhatSupp.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        private readonly IAccountService _accountService;

        public LoginViewModel(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public void Init()
        {
            var activeAccount = _accountService.GetActiveAccount();
            if (activeAccount != null)
                ShowViewModel<MainViewModel>();
        }

        private MvxCommand _loginCommand;
        public System.Windows.Input.ICommand LoginCommand
        {
            get
            {
                _loginCommand = _loginCommand ?? new MvxCommand(DoLogin);
                return _loginCommand;
            }
        }

        private void DoLogin()
        {
            ShowViewModel<MainViewModel>();
        }

        private MvxCommand _showCreateCommand;
        public System.Windows.Input.ICommand ShowCreateCommand
        {
            get
            {
                _showCreateCommand = _showCreateCommand ?? new MvxCommand(DoShowCreate);
                return _showCreateCommand;
            }
        }

        private void DoShowCreate()
        {
            ShowViewModel<CreateAccountViewModel>();
        }
    }
}
