using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using System.Linq;
using MvvmCross.Plugins.File;
using System.IO;
using System.Collections.Generic;
using WhatSupp.Services;

namespace WhatSupp.ViewModels
{
    public class ProductSearchViewModel : MvxViewModel
    {
        private readonly IRestService _restService;

        public ProductSearchViewModel(IRestService restService)
        {
            _restService = restService;
        }

        public string Term { get; set; }
        public List<Product> Results { get; set; }

        private MvxCommand _productSearchCommand;
        public System.Windows.Input.ICommand SearchProductCommand
        {
            get
            {
                _productSearchCommand = _productSearchCommand ?? new MvxCommand(DoProductSearch);
                return _productSearchCommand;
            }
        }

        private async void DoProductSearch()
        {
            Results = await _restService.SearchProducts(Term);
        }

        private MvxCommand<Product> _resultSelectedCommand;
        public System.Windows.Input.ICommand ResultSelectedCommand
        {
            get
            {
                _resultSelectedCommand = _resultSelectedCommand ?? new MvxCommand<Product>(DoSelectResult);
                return _resultSelectedCommand;
            }
        }

        private void DoSelectResult(Product product)
        {
            ShowViewModel<ViewProductViewModel>(new { id = product.ID });
        }
    }
}
