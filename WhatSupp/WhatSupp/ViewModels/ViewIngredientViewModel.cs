using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using System.Linq;
using MvvmCross.Plugins.File;
using System.IO;

namespace WhatSupp.ViewModels
{
    public class ViewIngredientViewModel : MvxViewModel
    {
        public void Init(Ingredient ingredient)
        {
            Ingredient = ingredient;
        }

        private Ingredient _ingredient;
        public Ingredient Ingredient
        {
            get { return _ingredient; }
            set { _ingredient = value; RaisePropertyChanged(() => Ingredient); }
        }
    }
}
