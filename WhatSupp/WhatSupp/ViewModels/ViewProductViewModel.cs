using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using System.Linq;
using MvvmCross.Plugins.File;
using System.IO;
using System;
using WhatSupp.Services;

namespace WhatSupp.ViewModels
{
    public class ViewProductViewModel : MvxViewModel
    {
        private readonly IRestService _restService;
        private readonly IFileService _fileService;
        private readonly IRepository<ProductScan> _scanRepo;

        public ViewProductViewModel(IRestService restService,
            IFileService fileService,
            IRepository<ProductScan> scanRepo)
        {
            _restService = restService;
            _fileService = fileService;
            _scanRepo = scanRepo;
        }

        public Product Product { get; set; }

        private double _rating;
        public double Rating
        {
            get
            {
                return System.Math.Round(_rating, 1);
            }
            set
            {
                _rating = value;
            }
        }
        public byte[] PictureBytes { get; set; }

        public async void Init(string path, int id)
        {
            if (!string.IsNullOrWhiteSpace(path))
                PictureBytes = _fileService.GetFileBytes(path);

            var rand = new System.Random();
            Rating = rand.Next(1, 5) + rand.NextDouble();

            if (id != 0)
            {
                Product = await _restService.GetProduct(id);
            }
            else
            {
                Product = await _restService.GetProductFromScan(PictureBytes);

                var scan = _scanRepo.Get(x => x.Path == path);
                
                if (scan == null)
                {
                    try
                    {
                        _scanRepo.Create(new ProductScan()
                        {
                            Path = path,
                            ProductID = Product.ID,
                            Name = Product.Name,
                            Date = DateTime.Now.ToString("MM'/'dd'/'yyyy")
                        });
                    }
                    catch (Exception e)
                    {

                        throw;
                    }
                }
            }
        }

        private MvxCommand<Ingredient> _ingredientSelectedCommand;
        public System.Windows.Input.ICommand IngredientSelectedCommand
        {
            get
            {
                _ingredientSelectedCommand = _ingredientSelectedCommand ?? new MvxCommand<Ingredient>(DoSelectIngredient);
                return _ingredientSelectedCommand;
            }
        }

        private void DoSelectIngredient(Ingredient ingredient)
        {
            ShowViewModel<ViewIngredientViewModel>(ingredient);
        }

        private MvxCommand _backCommand;
        public System.Windows.Input.ICommand BackCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    ShowViewModel<MainViewModel>();
                });
            }
        }
    }
}
