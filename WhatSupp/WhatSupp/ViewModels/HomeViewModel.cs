using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using WhatSupp.DataAccess;
using WhatSupp.Entities;

namespace WhatSupp.ViewModels
{
    public class HomeViewModel : MvxViewModel
    {
        public NewsFeedViewModel Vm1 { get; set; }
        public ProductScanListViewModel Vm2 { get; set; }
        public ProductSearchViewModel Vm3 { get; set; }

        public HomeViewModel()
        {
            Vm1 = (NewsFeedViewModel)Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(MvxViewModelRequest<NewsFeedViewModel>.GetDefaultRequest(), null);
            Vm2 = (ProductScanListViewModel)Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(MvxViewModelRequest<ProductScanListViewModel>.GetDefaultRequest(), null);
            Vm3 = (ProductSearchViewModel)Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(MvxViewModelRequest<ProductSearchViewModel>.GetDefaultRequest(), null);
        }
    }
}
