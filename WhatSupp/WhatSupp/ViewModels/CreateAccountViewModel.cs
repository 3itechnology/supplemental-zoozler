using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using WhatSupp.Services;

namespace WhatSupp.ViewModels
{
    public class CreateAccountViewModel : MvxViewModel
    {
        private readonly IAccountService _accountService;

        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public CreateAccountViewModel(IAccountService accountService)
        {
            _accountService = accountService;
        }

        private MvxCommand _createAccountCommand;
        public System.Windows.Input.ICommand CreateAccountCommand
        {
            get
            {
                _createAccountCommand = _createAccountCommand ?? new MvxCommand(DoCreateAccount);
                return _createAccountCommand;
            }
        }

        private void DoCreateAccount()
        {
            _accountService.CreateAccount(Email, Name, Password);
            ShowViewModel<MainViewModel>();
        }

        private MvxCommand _showLoginCommand;
        public System.Windows.Input.ICommand ShowLoginCommand
        {
            get
            {
                _showLoginCommand = _showLoginCommand ?? new MvxCommand(DoShowLogin);
                return _showLoginCommand;
            }
        }

        private void DoShowLogin()
        {
            ShowViewModel<LoginViewModel>();
        }
    }
}
