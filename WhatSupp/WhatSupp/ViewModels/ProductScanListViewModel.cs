using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using System.Linq;
using System.Collections.Generic;
using System;
using WhatSupp.Services;

namespace WhatSupp.ViewModels
{
    public class ProductScanListViewModel : MvxViewModel
    {
        private readonly IProductScanService _scanService;

        public ProductScanListViewModel(IProductScanService scanService)
        {
            _scanService = scanService;
            Scans = _scanService.GetScans();
        }

        private List<ProductScan> _scans;
        public List<ProductScan> Scans
        {
            get { return _scans; }
            set { _scans = value; RaisePropertyChanged(() => Scans); }
        }

        private MvxCommand<ProductScan> _itemSelectedCommand;
        public System.Windows.Input.ICommand ScanSelectedCommand
        {
            get
            {
                _itemSelectedCommand = _itemSelectedCommand ?? new MvxCommand<ProductScan>(DoSelectScan);
                return _itemSelectedCommand;
            }
        }

        private void DoSelectScan(ProductScan scan)
        {
            ShowViewModel<ViewProductViewModel>(new { path = scan.Path, id = scan.ProductID });
        }
    }
}
