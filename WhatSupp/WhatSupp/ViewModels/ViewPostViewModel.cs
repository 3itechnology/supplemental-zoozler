using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.DataAccess;
using WhatSupp.Entities;
using System.Linq;
using MvvmCross.Plugins.File;
using System.IO;

namespace WhatSupp.ViewModels
{
    public class ViewPostViewModel : MvxViewModel
    {
        public void Init(Post post)
        {
            Post = post;
        }

        private Post _post;
        public Post Post
        {
            get { return _post; }
            set { _post = value; RaisePropertyChanged(() => Post); }
        }
    }
}
