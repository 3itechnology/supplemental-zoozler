using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using WhatSupp.Entities;
using WhatSupp.DataAccess;
using System.Threading.Tasks;
using MvvmCross.Plugins.PictureChooser;
using MvvmCross.Plugins.File;
using System.IO;
using System;
using WhatSupp.Services;

namespace WhatSupp.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        private readonly IRestService _restService;
        private readonly IMvxPictureChooserTask _pictureService;
        private readonly IMvxFileStore _fileService;

        public MainViewModel(IRestService restService,
            IMvxPictureChooserTask pictureService,
            IMvxFileStore fileService)
        {
            _restService = restService;
            _pictureService = pictureService;
            _fileService = fileService;
        }

        public ICommand ViewProductCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    _pictureService.TakePicture(400, 95, OnLabelPictureTaken, () => { });
                });
            }
        }

        private void OnLabelPictureTaken(Stream pictureStream)
        {
            var memoryStream = new MemoryStream();
            pictureStream.CopyTo(memoryStream);
            var picBytes = memoryStream.ToArray();

            var id = Guid.NewGuid().ToString("N");

            var randomFileName = "Image" + id + ".jpg";
            _fileService.EnsureFolderExists("Images");
            var path = _fileService.PathCombine("Images", randomFileName);
            _fileService.WriteFile(path, picBytes);
            ShowViewModel<ViewProductViewModel>(new { path = path });
        }

        public ICommand ProductScanListCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    ShowViewModel<ProductScanListViewModel>();
                });
            }
        }
    }
}
