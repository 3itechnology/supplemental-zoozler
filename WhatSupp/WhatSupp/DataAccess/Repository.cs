﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Plugins.Sqlite;
using SQLite.Net;

namespace WhatSupp.DataAccess
{
    public interface IRepository<TEntity> where TEntity : class
    {
        List<TEntity> GetAll();
        List<TEntity> GetGroup(Func<TEntity, bool> predicate);
        TEntity Get(object id);
        TEntity Get(Func<TEntity, bool> predicate);
        int Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(object id);
        void Delete(TEntity entity);
        void DeleteAll();
        bool Exists(Func<TEntity, bool> predicate);
    }
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IMvxSqliteConnectionFactory _sqliteConnectionFactory;
        private readonly SQLiteConnection _connection;

        public Repository(IMvxSqliteConnectionFactory sqliteConnectionFactory)
        {
            _sqliteConnectionFactory = sqliteConnectionFactory;
            _connection = sqliteConnectionFactory.GetConnection("test.sql");
            _connection.CreateTable<TEntity>();
        }

        public int Create(TEntity entity)
        {
            return _connection.Insert(entity);
        }

        public void Delete(TEntity entity)
        {
            _connection.Delete(entity);
        }

        public void Delete(object id)
        {
            _connection.Delete(id);
        }

        public void DeleteAll()
        {
            _connection.DeleteAll<TEntity>();
        }

        public TEntity Get(Func<TEntity, bool> predicate)
        {
            return _connection.Table<TEntity>().FirstOrDefault(predicate);
        }

        public TEntity Get(object id)
        {
            return _connection.Get<TEntity>(id);
        }

        public List<TEntity> GetAll()
        {
            return _connection.Table<TEntity>().ToList();
        }

        public List<TEntity> GetGroup(Func<TEntity, bool> predicate)
        {
            return _connection.Table<TEntity>().Where(predicate).ToList();
        }

        public void Update(TEntity entity)
        {
            _connection.Update(entity);
        }

        public bool Exists(Func<TEntity, bool> predicate)
        {
            return _connection.Table<TEntity>().Any(predicate);
        }
    }
}
