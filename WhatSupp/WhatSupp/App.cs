using System;
using System.Collections.Generic;
using MvvmCross.Platform.IoC;
using WhatSupp.Entities;
using WhatSupp.DataAccess;
using MvvmCross.Platform;
using MvvmCross.Plugins.Sqlite;
using MvvmCross.Plugins.Sqlite.Droid;

namespace WhatSupp
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        private static IEnumerable<Type> MyTypes
        {
            get
            {
                //Return concrete types, NOT interfaces
                yield return typeof(Repository<ProductScan>);
                yield return typeof(Repository<Account>);
            }
        }

        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.RegisterType<IMvxSqliteConnectionFactory, DroidSqliteConnectionFactory>();
            MyTypes.AsInterfaces().RegisterAsLazySingleton();
            RegisterAppStart<ViewModels.LoginViewModel>();
        }
    }
}
