﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WhatSupp.Entities;

namespace WhatSupp.Services
{
    public interface IRestService
    {
        Task<List<Post>> GetPosts();
        Task<Product> GetProduct(int id);
        Task<List<Product>> SearchProducts(string term);
        Task<Product> GetProductFromScan(Byte[] imageData);
    }

    public class RestService : IRestService
    {
        private const string serviceUrl = @"http://104.236.96.224:8080/WhatSuppWeb/";
        private const string wpUrl = @"http://supplementalapp.com/api/get_category_posts/?category_slug=test&json=1";
        //private const string serviceUrl = @"http://192.168.200.28:8080/WhatSuppWeb/";

        public async Task<List<Post>> GetPosts()
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(wpUrl);
                var json = await result.Content.ReadAsStringAsync();
                var postList = JsonConvert.DeserializeObject<PostList>(json);
                return postList.Posts;
            }
        }

        public async Task<Product> GetProduct(int id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(serviceUrl + "products/" + id);
                var json = await result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Product>(json);
            }
        }

        public async Task<List<Product>> SearchProducts(string term)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(serviceUrl + "products/search/" + term);
                var json = await result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<Product>>(json);
            }
        }

        public async Task<Product> GetProductFromScan(Byte[] imageData)
        {
            using (var client = new HttpClient())
            {
                MultipartFormDataContent form = new MultipartFormDataContent();
                form.Add(new ByteArrayContent(imageData, 0, imageData.Count()), "file", "helloworld.jpg");
                var response = await client.PostAsync(serviceUrl + "upload/", form);
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Product>(json);
            }
        }
    }
}
