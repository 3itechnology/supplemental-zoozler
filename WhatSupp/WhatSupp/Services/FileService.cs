﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Plugins.File;

namespace WhatSupp.Services
{
    public interface IFileService
    {
        byte[] GetFileBytes(string path);
    }

    public class FileService : IFileService
    {
        private readonly IMvxFileStore _fileStore;

        public FileService(IMvxFileStore fileStore)
        {
            _fileStore = fileStore;
        }

        public byte[] GetFileBytes(string path)
        {
            using (var memstream = new MemoryStream())
            {
                _fileStore.OpenRead(path).CopyTo(memstream);
                return memstream.ToArray();
            }
        }
    }
}
