﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Plugins.File;
using WhatSupp.DataAccess;
using WhatSupp.Entities;

namespace WhatSupp.Services
{
    public interface IProductScanService
    {
        List<ProductScan> GetScans();
    }

    public class ProductScanService : IProductScanService
    {
        private readonly IFileService _fileService;
        private readonly IRepository<ProductScan> _scanRepo;

        public ProductScanService(IFileService fileService,
            IRepository<ProductScan> scanRepo)
        {
            _fileService = fileService;
            _scanRepo = scanRepo;
        }

        public List<ProductScan> GetScans()
        {
            var scans = _scanRepo.GetAll().Take(10).ToList();

            // fill byte data for scans
            foreach (var scan in scans)
            {
                scan.PictureBytes = _fileService.GetFileBytes(scan.Path);
            }

            return scans;
        }
    }
}
