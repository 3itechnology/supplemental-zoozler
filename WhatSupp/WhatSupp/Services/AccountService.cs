﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Plugins.File;
using WhatSupp.DataAccess;
using WhatSupp.Entities;

namespace WhatSupp.Services
{
    public interface IAccountService
    {
        Account GetAccount(string email);
        Account GetActiveAccount();
        void CreateAccount(string email, string name, string password);
        bool Login(string email, string password);
        void Logout(string email);
    }

    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> _accountRepo;

        public AccountService(IRepository<Account> accountRepo)
        {
            _accountRepo = accountRepo;
        }

        public Account GetAccount(string email)
        {
            return _accountRepo.Get(email);
        }

        public Account GetActiveAccount()
        {
            return _accountRepo.Get(x => x.Active == true);
        }

        public void CreateAccount(string email, string name, string password)
        {
            var user = new Account()
            {
                Email = email,
                Name = name,
                Password = password,
                Active = true
            };
            _accountRepo.Create(user);
        }

        public bool Login(string email, string password)
        {
            var user = _accountRepo.Get(x => x.Email == email && x.Password == password);
            
            if (user != null)
            {
                user.Active = true;
                _accountRepo.Update(user);
                return true;
            }

            return false;
        }

        public void Logout(string email)
        {
            var user = _accountRepo.Get(email);
            user.Active = false;
            _accountRepo.Update(user);
        }
    }
}
