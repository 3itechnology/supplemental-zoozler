﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.Text;
using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace WhatSupp.Entities
{
    public class Account
    {
        [PrimaryKey]
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
    }
}
