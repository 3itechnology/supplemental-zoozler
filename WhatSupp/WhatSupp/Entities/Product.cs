﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace WhatSupp.Entities
{
    public class Product
    {
        [PrimaryKey]
        [JsonProperty(PropertyName = "id")]
        public int ID { get; set; }
        
        [JsonProperty(PropertyName="upc")]
        public string UPC { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public string Brand { get; set; }
        
        [JsonProperty(PropertyName = "rating")]
        public string Rating { get; set; }

        public List<Ingredient> Ingredients { get; set; }

        public override string ToString()
        {
            return Brand + " " + Name;
        }
    }
}
