﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.Text;
using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace WhatSupp.Entities
{
    public class Post : Test
    {
        [JsonProperty(PropertyName = "content")]
        public string Content { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string Date { get; set; }

        public string DateFormatted
        {
            get
            {
                var date = Convert.ToDateTime(Date);
                return date.ToString("MM'/'dd'/'yyyy");
            }
        }

        [JsonProperty(PropertyName = "excerpt")]
        public string Excerpt { get; set; }

        public ISpanned HtmlContent
        {
            get
            {
                return Html.FromHtml(Content);
            }
        }

        public ISpanned HtmlExcerpt
        {
            get
            {
                return Html.FromHtml(Excerpt);
            }
        }

        public ISpanned HtmlTitle
        {
            get
            {
                return Html.FromHtml(Title);
            }
        }

        [PrimaryKey]
        [JsonProperty(PropertyName = "id")]
        public int ID { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string URL { get; set; }
    }

    public class TestAd : Test
    {
        public string Test { get; set; }
    }

    public class Test
    {

    }

    public class PostList
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "posts")]
        public List<Post> Posts { get; set; }
    }
}
