﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace WhatSupp.Entities
{
    public class Ingredient
    {
        [PrimaryKey]
        [JsonProperty(PropertyName="id")]
        public int ID { get; set; }

        [JsonProperty(PropertyName = "labelName")]
        public string LabelName { get; set; }

        [JsonProperty(PropertyName = "baseName")]
        public string BaseName { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        public override string ToString()
        {
            return string.Format("{0} ({1})", BaseName, LabelName);
        }
    }
}
