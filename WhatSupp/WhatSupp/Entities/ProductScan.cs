﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace WhatSupp.Entities
{
    public class ProductScan
    {
        [PrimaryKey]
        [AutoIncrement]
        public int ID { get; set; }
        [Ignore]
        public byte[] PictureBytes { get; set; }
        public string Date { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int ProductID { get; set; }
        public int UserID { get; set; }
    }
}
