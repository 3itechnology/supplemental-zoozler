using MvvmCross.Platform.Plugins;

namespace WhatSupp.Droid.Bootstrap
{
    public class FilePluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader>
    {
    }
}