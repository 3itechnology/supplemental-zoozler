using MvvmCross.Platform.Plugins;

namespace WhatSupp.Droid.Bootstrap
{
    public class PictureChooserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.PictureChooser.PluginLoader>
    {
    }
}