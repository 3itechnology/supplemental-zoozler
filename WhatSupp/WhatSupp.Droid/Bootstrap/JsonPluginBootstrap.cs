using MvvmCross.Platform.Plugins;

namespace WhatSupp.Droid.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}