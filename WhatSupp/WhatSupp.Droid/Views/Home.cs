using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V7.Fragging;
using WhatSupp.ViewModels;
using System.Collections.Generic;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using V4Fragment = Android.Support.V4.App.Fragment;
using V4FragmentManager = Android.Support.V4.App.FragmentManager;
using MvvmCross.Droid.Support.V7.Fragging.Fragments;

namespace WhatSupp.Droid.Views
{
    [Activity]
    public class Home : MvxFragment
    {
        TextView lblMessage;
        DrawerLayout drawer;
        Android.Support.V7.Widget.Toolbar toolbar;
         
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.Home, null);

            var navigationView = view.FindViewById<NavigationView>(Resource.Id.nav_view);
            if (navigationView != null)
                setupDrawerContent(navigationView);

            // add a "hamburger" menu button for the nav drawer
            toolbar = (Android.Support.V7.Widget.Toolbar)view.FindViewById(Resource.Id.toolbar);
            drawer = (DrawerLayout)Activity.FindViewById(Resource.Id.drawer_layout);
            var toggle = new Android.Support.V7.App.ActionBarDrawerToggle(
                    this.Activity, drawer, toolbar, Resource.String.drawer_open, Resource.String.drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            toolbar.Title = "SuppleMENTAL";

            var viewPager = view.FindViewById<Android.Support.V4.View.ViewPager>(Resource.Id.viewpager);
            if (viewPager != null)
            {
                var fragments = new List<MvxViewPagerFragmentAdapter.FragmentInfo>
                {
                    new MvxViewPagerFragmentAdapter.FragmentInfo
                    {
                        FragmentType = typeof(NewsFeed),
                        Icon = Resource.Drawable.news,
                        ViewModel = (ViewModel as HomeViewModel).Vm1
                    },
                    new MvxViewPagerFragmentAdapter.FragmentInfo
                    {
                        FragmentType = typeof(ProductScanList),
                        Icon = Resource.Drawable.news,
                        ViewModel = (ViewModel as HomeViewModel).Vm2
                    },
                    new MvxViewPagerFragmentAdapter.FragmentInfo
                    {
                        FragmentType = typeof(ProductSearch),
                        Icon = Resource.Drawable.news,
                        ViewModel = (ViewModel as HomeViewModel).Vm3
                    }
                };
                viewPager.Adapter = new MvxViewPagerFragmentAdapter(Activity, ChildFragmentManager, fragments);
            }
            viewPager.OffscreenPageLimit = 3;
            viewPager.PageSelected += ViewPager_PageSelected;

            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            tabLayout.SetupWithViewPager(viewPager);

            // set icons
            tabLayout.GetTabAt(0).SetIcon(Resource.Drawable.news);
            tabLayout.GetTabAt(1).SetIcon(Resource.Drawable.pills);
            tabLayout.GetTabAt(2).SetIcon(Resource.Drawable.search);

            return view;
        }

        private void ViewPager_PageSelected(object sender, Android.Support.V4.View.ViewPager.PageSelectedEventArgs e)
        {
            switch (e.Position)
            {
                case 0: toolbar.Title = "SuppleMENTAL";
                    break;
                case 1:
                    toolbar.Title = "My Supplements";
                    break;
                case 2:
                    toolbar.Title = "Supplement Library";
                    break;
            }
        }

        void setupDrawerContent(NavigationView navigationView)
        {
            navigationView.NavigationItemSelected += (sender, e) =>
            {
                e.MenuItem.SetChecked(true);
                drawer.CloseDrawers();

                switch (e.MenuItem.ItemId)
                {
                    case Resource.Id.nav_home:
                        var viewModel = (MainViewModel)ViewModel;
                        viewModel.ProductScanListCommand.Execute(null);
                        break;
                }
            };
        }

        class Adapter : Android.Support.V4.App.FragmentPagerAdapter
        {
            List<V4Fragment> fragments = new List<V4Fragment>();
            List<string> fragmentTitles = new List<string>();

            public Adapter(V4FragmentManager fm) : base(fm)
            {
            }

            public void AddFragment(V4Fragment fragment, string title)
            {
                fragments.Add(fragment);
                fragmentTitles.Add(title);
            }

            public override V4Fragment GetItem(int position)
            {
                return fragments[position];
            }

            public override int Count
            {
                get { return fragments.Count; }
            }

            public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
            {
                return new Java.Lang.String(fragmentTitles[position]);
            }
        }
    }
}
