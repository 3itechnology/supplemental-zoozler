using System;
using Android.Animation;
using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using MvvmCross.Droid.Support.V7.Fragging;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;
using WhatSupp.ViewModels;
using ZXing.Mobile;
using Android.Content;

namespace WhatSupp.Droid.Views
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Main : MvxFragmentActivity
    {
        bool isFabOpen = false;
        FloatingActionButton fab, fabScan, fab2, fab3, fab4, fab5;
        RelativeLayout content;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            
            // initialize barcode scanner
            MobileBarcodeScanner.Initialize(Application);


            content = (RelativeLayout)FindViewById(Resource.Id.main_content);

            fab = (FloatingActionButton)FindViewById(Resource.Id.fab);
            fabScan = (FloatingActionButton)FindViewById(Resource.Id.fab1);
            fab2 = (FloatingActionButton)FindViewById(Resource.Id.fab2);
            fab3 = (FloatingActionButton)FindViewById(Resource.Id.fab3);
            fab4 = (FloatingActionButton)FindViewById(Resource.Id.fab4);
            fab5 = (FloatingActionButton)FindViewById(Resource.Id.fab5);
            
            fabScan.Click += fabScan_Click;

            // set custom presenter
            var presenter = (DroidPresenter)Mvx.Resolve<IMvxAndroidViewPresenter>();
            var initialFragment = new Home { ViewModel = Mvx.IocConstruct<HomeViewModel>() };

            presenter.RegisterFragmentManager(SupportFragmentManager, initialFragment);
        }

        private async void fabScan_Click(object sender, System.EventArgs e)
        {
            CollapseFabs();

            var viewModel = (MainViewModel)ViewModel;

            //var scanner = new ZXing.Mobile.MobileBarcodeScanner();
            //var result = await scanner.Scan();

            //if (result != null)
            //{
            //    viewModel.ScannedUPC = result.Text;
            //    viewModel.AddEditCommand.Execute(null);
            //}
        }
        
        private void MoveFabOut(FloatingActionButton fab, int x, int y)
        {
            fab.Visibility = Android.Views.ViewStates.Visible;
            ObjectAnimator moverX = ObjectAnimator.OfFloat(fab, "translationX", 0, x);
            moverX.SetDuration(300);
            moverX.Start();
            ObjectAnimator moverY = ObjectAnimator.OfFloat(fab, "translationY", 0, y);
            moverY.SetDuration(300);
            moverY.Start();
            ObjectAnimator scalerX = ObjectAnimator.OfFloat(fab, "scaleX", 0, 1.2f);
            scalerX.SetDuration(350);
            scalerX.Start();
            ObjectAnimator scalerY = ObjectAnimator.OfFloat(fab, "scaleY", 0, 1.2f);
            scalerY.SetDuration(350);
            scalerY.Start();
            ObjectAnimator alphaer = ObjectAnimator.OfFloat(fab, "alpha", 0, 1.0f);
            alphaer.SetDuration(300);
            alphaer.Start();
            ObjectAnimator scalerX2 = ObjectAnimator.OfFloat(fab, "scaleX", 1.2f, 1.0f);
            scalerX2.SetDuration(200);
            scalerX2.StartDelay = 350;
            scalerX2.Start();
            ObjectAnimator scalerY2 = ObjectAnimator.OfFloat(fab, "scaleY", 1.2f, 1.0f);
            scalerY2.SetDuration(200);
            scalerY2.StartDelay = 350;
            scalerY2.Start();
        }

        private void MoveFabIn(FloatingActionButton fab, int x, int y)
        {
            fab.Visibility = Android.Views.ViewStates.Visible;
            ObjectAnimator scalerX2 = ObjectAnimator.OfFloat(fab, "scaleX", 1.0f, 1.2f);
            scalerX2.SetDuration(200);
            scalerX2.Start();
            ObjectAnimator scalerY2 = ObjectAnimator.OfFloat(fab, "scaleY", 1.0f, 1.2f);
            scalerY2.SetDuration(200);
            scalerY2.Start();
            ObjectAnimator moverX = ObjectAnimator.OfFloat(fab, "translationX", x, 0);
            moverX.SetDuration(300);
            moverX.StartDelay = 200;
            moverX.Start();
            ObjectAnimator moverY = ObjectAnimator.OfFloat(fab, "translationY", y, 0);
            moverY.SetDuration(300);
            moverY.StartDelay = 200;
            moverY.Start();
            ObjectAnimator scalerX = ObjectAnimator.OfFloat(fab, "scaleX", 1.2f, 0);
            scalerX.SetDuration(350);
            scalerX.StartDelay = 200;
            scalerX.Start();
            ObjectAnimator scalerY = ObjectAnimator.OfFloat(fab, "scaleY", 1.2f, 0);
            scalerY.SetDuration(350);
            scalerY.StartDelay = 200;
            scalerY.Start();
            ObjectAnimator alphaer = ObjectAnimator.OfFloat(fab, "alpha", 1.0f, 0);
            alphaer.SetDuration(300);
            alphaer.StartDelay = 200;
            alphaer.Start();
        }

        private void FanFabs()
        {
            isFabOpen = true;
            MoveFabOut(fabScan, -400, 0);
            MoveFabOut(fab2, -370, -153);
            MoveFabOut(fab3, -283, -283);
            MoveFabOut(fab4, -153, -370);
            MoveFabOut(fab5, 0, -400);

            ObjectAnimator animator = ObjectAnimator.OfInt(content, "backgroundColor", Color.ParseColor("#00000000"), Color.ParseColor("#BB111111"));
            animator.SetEvaluator(new ArgbEvaluator());
            animator.SetDuration(300);
            animator.Start();
        }

        private void CollapseFabs()
        {
            isFabOpen = false;
            MoveFabIn(fabScan, -400, 0);
            MoveFabIn(fab2, -370, -153);
            MoveFabIn(fab3, -283, -283);
            MoveFabIn(fab4, -153, -370);
            MoveFabIn(fab5, 0, -400);

            ObjectAnimator animator = ObjectAnimator.OfInt(content, "backgroundColor", Color.ParseColor("#BB111111"), Color.ParseColor("#00000000"));
            animator.SetEvaluator(new ArgbEvaluator());
            animator.SetDuration(300);
            animator.StartDelay = 200;
            animator.Start();
        }

        public override void OnBackPressed()
        {
            int count = SupportFragmentManager.BackStackEntryCount;

            if (count == 0)
            {
                //set alert for executing the task
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Leave?");
                alert.SetMessage("Are you sure you want to exit?");
                alert.SetPositiveButton("Yes", (senderAlert, args) => {
                    Finish();
                });
                alert.SetNegativeButton("No", (EventHandler<DialogClickEventArgs>)null);

                Dialog dialog = alert.Create();
                dialog.Show();
            }
            else
            {
                SupportFragmentManager.PopBackStack();
            }
        }
    }
}
