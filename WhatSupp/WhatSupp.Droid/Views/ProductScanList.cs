using Android.OS;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V7.Fragging.Fragments;
using Android.Runtime;
using WhatSupp.ViewModels;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using Android.Widget;
using Android.Support.V4.Widget;

namespace WhatSupp.Droid.Views
{
    public class ProductScanList : MvxFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.product_scan_list_view, null);
            
            return view;
        }
    }
}
