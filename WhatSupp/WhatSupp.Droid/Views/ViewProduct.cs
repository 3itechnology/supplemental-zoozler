using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.BindingContext;
using WhatSupp.ViewModels;
using System.Linq;
using MvvmCross.Droid.Support.V7.Fragging.Fragments;
using Android.Support.V7.Widget;

namespace WhatSupp.Droid.Views
{
    public class ViewProduct : MvxFragment
    {
        ListView listView;
        View view;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = this.BindingInflate(Resource.Layout.ViewProductView, null);
            //listView = (ListView)view.FindViewById(Resource.Id.list);

            //var product = (ViewModel as ViewProductViewModel).product;
            //var ingredientNames = (from x in product.Ingredients select x.Name).ToArray();

            //ArrayAdapter<string> adapter = new ArrayAdapter<string>(this.Context, Android.Resource.Layout.SimpleListItem1, ingredientNames);

            //loadBackdrop();

            return view;
        }

        void loadBackdrop()
        {
            var imageView = view.FindViewById<ImageView>(Resource.Id.backdrop);

            var r = Resource.Drawable.supplements;
            imageView.SetImageResource(r);
        }
    }
}
