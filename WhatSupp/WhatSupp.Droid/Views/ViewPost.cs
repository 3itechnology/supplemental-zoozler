using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.BindingContext;
using WhatSupp.ViewModels;
using System.Linq;
using MvvmCross.Droid.Support.V7.Fragging.Fragments;
using Android.Support.V7.Widget;
using Android.Webkit;

namespace WhatSupp.Droid.Views
{
    public class ViewPost : MvxFragment
    {
        View view;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = this.BindingInflate(Resource.Layout.ViewPost, null);
            LoadHtml();
            return view;
        }

        void LoadHtml()
        {
            WebView webView = (WebView)view.FindViewById(Resource.Id.webView);
            webView.Settings.JavaScriptEnabled = true;
            webView.LoadUrl((ViewModel as ViewPostViewModel).Post.URL);
        }
    }
}
