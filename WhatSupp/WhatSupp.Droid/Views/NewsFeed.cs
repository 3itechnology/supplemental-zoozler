using Android.OS;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V7.Fragging.Fragments;
using Android.Runtime;
using WhatSupp.ViewModels;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using Android.Widget;
using Android.Support.V4.Widget;
using Android.Gms.Ads;
using MvvmCross.Binding.Droid.Views;
using Android.Content;
using WhatSupp.Entities;

namespace WhatSupp.Droid.Views
{
    public class NewsFeed : MvxFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.NewsFeed, null);
            
            view.ViewTreeObserver.ScrollChanged += ViewTreeObserver_ScrollChanged;

            var adView = view.FindViewById<NativeExpressAdView>(Resource.Id.adView);

            AdRequest request = new AdRequest.Builder().Build();

            adView.LoadAd(request);


            var list = view.FindViewById<MvxListView>(Resource.Id.news_list);
            list.Adapter = new CustomAdapter(Activity, (IMvxAndroidBindingContext)BindingContext);
            


            return view;
        }

        private void ViewTreeObserver_ScrollChanged(object sender, EventArgs e)
        {
            //var scrollView = Activity.FindViewById<NestedScrollView>(Resource.Id.news_scroll);
            //var container = Activity.FindViewById<LinearLayout>(Resource.Id.news_container);

            //if (container.ChildCount > 1)
            //{
            //    var y = scrollView.ScrollY;
            //    var height = scrollView.Height;

            //    var lastChild = container.GetChildAt(container.ChildCount - 1);

            //    int diff = (lastChild.Bottom - (height + y));

            //    if (diff == 0)
            //        AddNewsItems();
            //}
        }

        public class CustomAdapter : MvxAdapter
        {
            public CustomAdapter(Context context, IMvxAndroidBindingContext bindingContext)
                : base(context, bindingContext)
            {
            }

            public override int GetItemViewType(int position)
            {
                var item = GetRawItem(position);
                if (item is TestAd)
                    return 1;
                return 0;
            }

            public override int ViewTypeCount
            {
                get { return 2; }
            }

            protected override View GetBindableView(View convertView, object source, int templateId)
            {
                if (source is TestAd)
                {
                    templateId = Resource.Layout.x_ad_item;

                    var view = base.GetBindableView(convertView, source, templateId);

                    var adView = view.FindViewById<NativeExpressAdView>(Resource.Id.adView);

                    AdRequest request = new AdRequest.Builder().Build();

                    adView.LoadAd(request);
                }
                else if (source is Post)
                    templateId = Resource.Layout.news_feed_item;

                return base.GetBindableView(convertView, source, templateId);
            }
        }
    }
}
