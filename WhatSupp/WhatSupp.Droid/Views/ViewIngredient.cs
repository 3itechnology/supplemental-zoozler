using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.BindingContext;
using WhatSupp.ViewModels;
using System.Linq;
using MvvmCross.Droid.Support.V7.Fragging.Fragments;

namespace WhatSupp.Droid.Views
{
    public class ViewIngredient : MvxFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.ViewIngredient, null);
            return view;
        }
    }
}
